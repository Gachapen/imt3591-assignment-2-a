Open the index.html file in a browser to run the program.
! Some older browsers might not work very well. Newest releases of all major browsers are known to work fine.!

- Click on a node to start creating a path. A red line will indicate the path. Click on another to create a path between them. Click on an empty space to create a new node with a path to it. Press ESC to exit the creating mode.

- Click on a node and press DELETE to remove the node and all of its paths.

- To delete a path between two nodes, click the first, then the other. (Just like creating a path)

- You can set the weight created paths will get by using the control field saying so.

- The rest should be obvious. If not, cantact one of us.
