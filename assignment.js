// The representation of a weighted path from one node to another.
function NodePath( weight, node) {
	this.weight = weight;
	this.node = node;
}

// The representation of a node in the graph.
function Node(col, row) {
	this.col = col;
	this.row = row;
	this.id = col + "_" + row;
	this.radius = 10;
	this.color = "#ffff00";
	this.relations = new Array();

	this.colorReset = function() {
		this.color = "#ffff00";
	}

	// Draw a node in the specified grid on specified context with specified color.
	this.draw = function( context, grid, fillColor ) {
		context.save();
		context.fillStyle = this.color;
		if( fillColor ) {
			context.fillStyle = fillColor;
		}

		context.beginPath();
		var renderCoordinate = grid.getCoordinateFromCell(new Vector(col, row));
		context.arc(renderCoordinate.x, renderCoordinate.y, this.radius, 0, Math.PI*2, true );
		context.closePath();
		context.fill();
		context.stroke();

		context.restore();
	}

	// Draw a path to a specific node in specified grid on specified context with specified color.
	this.drawPathTo = function(node, context, grid, strokeColor) {
		context.save();

		if( strokeColor ) {
			context.strokeStyle = strokeColor;
		}

		var renderCoordinate = grid.getCoordinateFromCell(new Vector(col, row));
		var relationRenderCoordinate = grid.getCoordinateFromCell(new Vector(node.col, node.row));

		context.beginPath();
		context.moveTo(renderCoordinate.x, renderCoordinate.y);
		context.lineTo(relationRenderCoordinate.x, relationRenderCoordinate.y);
		context.stroke();

		context.restore();
	}

	// Draw path to all relations in specified grid on specified context with specified color.
	this.drawRelations = function( context, grid, strokeColor ) {
		for( var index in this.relations ) {
			this.drawPathTo(this.relations[index].node, context, grid, strokeColor);
		}
	}

	// Draw the weights of every path from the node in the specified frid on specified context.
	this.drawWeights = function( context, grid ) {
		context.font="24px Arial";
		for( var index in this.relations ) {
			startCoordinate = grid.getCoordinateFromCell( new Vector( col, row ) );
			endCoordinate = grid.getCoordinateFromCell( new Vector( this.relations[index].node.col,
										this.relations[index].node.row ) );
			var textCoordinate = new Vector();
			textCoordinate.x = ( startCoordinate.x + endCoordinate.x ) / 2;
			textCoordinate.y = ( startCoordinate.y + endCoordinate.y ) / 2;

			context.fillText( this.relations[index].weight, textCoordinate.x, textCoordinate.y );
		}
	}

	this.removeRelations = function()
	{
		// Go through all its relations.
		for(var i in this.relations) {

			// Get the related node.
			var nodeB = this.relations[i].node;

			// Go through the second node's relation.
			for( var j in nodeB.relations) {

				// Find the second nodes relation to the first node.
				if(nodeB.relations[j].node === this) {
					
					// Delete the relation.
					 nodeB.relations.splice(j, 1);
				}
			}
		}

		// Delete all my relations.
		this.relations.splice(0, this.relations.length);
	}
} // end Node()

// The representation of a graph containing nodes.
function Graph() {
	this.nodes = new Array();

	this.renderNodes = function(context, grid) {
		for( var index in this.nodes ) {
			this.nodes[index].draw(context, grid);
		}
	}

	this.renderPaths = function(context, grid) {
		var strokeColor = context.strokeStyle;
		var strokeWidth = context.lineWidth;
		context.strokeStyle = "#999999";
		context.lineWidth = 3;

		for( var index in this.nodes ) {
			this.nodes[index].drawRelations(context, grid);
		}
		context.strokeStyle = strokeColor;
		context.lineWidth = strokeWidth;
	}

	this.renderWeights = function( context, grid ) {
		for( var index in this.nodes ) {
			this.nodes[index].drawWeights(context, grid);
		}
	}

	// Initializes the graph and loads the graph nodes from the script element with id="Assignment2_graph".
	// (warning: this is ugly)
	this.init = function( defaultNodeColor ) {
		var rawGraphInfo = document.getElementById("Assignment2_graph");
		if( ! rawGraphInfo ) {
			return null;
		}
		var str = "";
		var element = rawGraphInfo.firstChild;
		while( element ) {
			if( element.nodeType == 3 ) {
				str = element.textContent;
				var lines = str.split("\n");
				for( var i = 0; i < lines.length; i++ ) {
					if( lines[i][0] == "#" ) {
					// this is a comment, do nothing
					} else {
						// submitting nodes with a path
						var values = lines[i].split(" ");
						var data = new Array();
						for( var index in values ) {
							if( values[index] != "" ) {
								data.push( values[index] );
							}
						}
						if( data.length == 5 ) {
							// valid input:
							// 	- both nodes: find old or create new
							// 	- create path between them
							// 	- bind paths to nodes
							// 	data[2] = weight between nodes
							var strStartX_StartY = data[0] + "_" + data[1];
							var startNode = this.nodes[strStartX_StartY];
							if( !startNode ) {
								startNode = new Node( data[0], data[1], strStartX_StartY );
								startNode.color = defaultNodeColor;
								this.nodes[strStartX_StartY] = startNode;
							}
	
							var strDestX_DestY = data[3] + "_" + data[4];
							var destNode = this.nodes[strDestX_DestY];
							if( !destNode ) {
								destNode = new Node( data[3], data[4], strDestX_DestY );
								destNode.color = defaultNodeColor;
								this.nodes[strDestX_DestY] = destNode;
							}

							var pathToDest = new NodePath( parseFloat(data[2]), destNode );
							var pathToStart = new NodePath( parseFloat(data[2]), startNode );
							startNode.relations.push( pathToDest );
							destNode.relations.push( pathToStart );
						} // end if valid input
					} // end else(when read line is not a comment)
				} // end for each line
			} // end if( element is of type text)
			element = element.nextSibling;
		} // end while( read from file)
	} // end init()
} // end Graph()

// The representation of the environment grid.
function Grid(numRows, numColumns) {
	var rowHeight = null;
	var columnWidth = null;

	this.render = function(context) {
		rowHeight = context.canvas.height / numRows;
		columnWidth = context.canvas.width / numColumns;

		for (var row = 0; row <= numRows; row++) {
			context.beginPath();
			context.moveTo(0, row * rowHeight);
			context.lineTo(context.canvas.width, row * rowHeight);
			context.stroke();
		}
		
		for (var column = 0; column <= numColumns; column++) {
			context.beginPath();
			context.moveTo(column * columnWidth, 0);
			context.lineTo(column * columnWidth, context.canvas.height);
			context.stroke();
		}
	}

	// Returns a Vector object with column(x) and row(y) of the cell of the canvas coordinate.
	this.getCellFromCoordinate = function(mousePosition) {
		var validNodePosition = new Vector(0, 0);

		validNodePosition.x = Math.floor(mousePosition.x / columnWidth);
		validNodePosition.y = Math.floor(mousePosition.y / rowHeight);		

		// If cell is not part of the grid.
		if (validNodePosition.x > numColumns - 1 || validNodePosition.x < 0) {
			return null;
		}
	    if (validNodePosition.y > numRows - 1 || validNodePosition.y < 0) {
			return null;
		}
		
		return validNodePosition;
	}

	// Returns a Vector containing the x and y coordinate of the center of specified cell.
	this.getCoordinateFromCell = function(cellPosition) {
		var coordinate = new Vector(0, 0);

		coordinate.x = cellPosition.x * columnWidth;
		coordinate.y = cellPosition.y * rowHeight;

		if (coordinate.x > numColumns * columnWidth || coordinate.x < 0) {
			return null;
		}
	
		if (coordinate.y > numRows * rowHeight || coordinate.y < 0) {
			return null;
		}

		coordinate.x += columnWidth * 0.5;
		coordinate.y += rowHeight * 0.5;

		return coordinate;
	}
}

// The assignemt program.
function Assignment(width, height) {
	var canvas = document.getElementById('programCanvas');
	var context = canvas.getContext('2d');
	
	var numRows = 15;
	var numColumns = 15;
	var defaultWeight = 1.0;

	var grid = new Grid( numRows, numColumns );
	var graph = new Graph();

	var curMousePosition = new Vector(0, 0);
	var curModifyNodeVector = null;

	var firstOfPair = null;
	var firstNode = true;
	var nodeOnePosition = null;
	var nodeTwoPosition = null;

	// Variables used for animating the path walking.
	var currentAnimationIndex = 0;
	var animationIntervalId = null;

	var defaultNodeColor = "#ffff00";
	var startNode = null;
	var startNodeColor = "#00ff00";
	var goalNode = null;
	var goalNodeColor = "#8888ff";
	var buttonSetNode = null;

	// Initialize the program.
	this.init = function() {
		graph.init( defaultNodeColor );

		startNode = graph.nodes["0_0"];
		startNode.color = startNodeColor;
		goalNode = graph.nodes["14_9"];
		goalNode.color = goalNodeColor;

		canvas.addEventListener('mousedown', onMousePress,false);
		canvas.addEventListener('mousemove',onMouseMove ,false);
		document.addEventListener('keydown', onKeyPress, true);
		window.addEventListener('resize', onResize, false);
		window.addEventListener('orientationchange', onResize, false);
		document.getElementById('weight').placeholder = "Default: "+defaultWeight+" (value.decimal)";
		document.getElementById('runAlgorithm').addEventListener('click', runSearchAlgorithm, false);
		document.getElementById('setStartNode').addEventListener('click', function() {
			setNode( "start" );
		}, false );
		document.getElementById('setGoalNode').addEventListener('click', function () {
			setNode( "goal" );
		}, false );
		document.getElementById('setStartNode').style.backgroundColor = startNodeColor;
		document.getElementById('setGoalNode').style.backgroundColor = goalNodeColor;
		
		onResize();
		render();
	}

	// Render the whole program.
	var render = function() {
		context.clearRect(0, 0, canvas.width, canvas.height);
		grid.render(context);
		graph.renderPaths( context, grid );
		graph.renderNodes( context, grid );
		graph.renderWeights( context, grid );
	}

	// Render the creation of a path (when draggin a red path line from a node).
	var renderPathCreation = function() {
		var validModifyVector = grid.getCoordinateFromCell(curModifyNodeVector);

		if(validModifyVector)
		{
			context.save();
			context.strokeStyle = "#ff0000";
			context.save();
			context.beginPath();
	
			context.moveTo(validModifyVector.x, validModifyVector.y);
			context.restore();
			context.lineTo(curMousePosition.x, curMousePosition.y);
	
			context.stroke();
			context.restore();
		}
	}

	// Celled when clicking the run button. Does the algorithm and animates the path.
	var runSearchAlgorithm = function() {
		var algorithm = document.getElementById('algorithm').value;
		var resultPath = null;
		currentAnimationIndex = 0;

		switch (algorithm) {
		case "aStarTree":
			resultPath = aStarTreeSeach(startNode, goalNode, heuristics);
			break;
		case "aStarGraph":
			resultPath = aStarGraphSeach(startNode, goalNode, heuristics);
			break;
		}

		// Bug: Another click while animating will make the interval last forever and therefore double the speed.
		animationIntervalId = setInterval(function() {
			if (currentAnimationIndex < resultPath.length - 1) {
				render();
				var lineWidth = context.lineWidth;
				context.lineWidth = lineWidth * 4;
				resultPath[currentAnimationIndex].drawPathTo(resultPath[currentAnimationIndex + 1], context, grid, "#ff0000");
				context.lineWidth = lineWidth;
				currentAnimationIndex++;
			} else {
				clearInterval(animationIntervalId);
				render();
			}
		}, 500);
	}

	/* current vector is the node we are currently working on
	 * wheras the modify vector is the previous node which
	 * is to ble linked together
	 * */
	var editPath = function(currentVector)
	{
		var curVector = currentVector;
		var value = 25;
		var inWeight = parseFloat(document.getElementById("weight").value);
		if( !inWeight ) {
			inWeight = defaultWeight;
		}

		// The first(starting)node on the graph's path.
		var currentModifyIndex = curModifyNodeVector.x+"_"+curModifyNodeVector.y;
		var currentIndex = curVector.x+"_"+curVector.y;

		var pathToFirstNode = null;
		var pathToSecondNode = null;

		var index = 0;
		
		while( index < graph.nodes[currentModifyIndex].relations.length && !pathToFirstNode )
		{
			// Found node B in node A
			if( graph.nodes[currentModifyIndex].relations[index].node == graph.nodes[currentIndex] ) 
			{
				var secondIndex = 0;

				while( secondIndex < graph.nodes[currentIndex].relations.length && !pathToSecondNode )
				{
					if( graph.nodes[currentIndex].relations[secondIndex].node == graph.nodes[currentModifyIndex] )
					{
						pathToSecondNode = graph.nodes[currentIndex].relations[secondIndex];
					}
					secondIndex++;
				}
				pathToFirstNode = graph.nodes[currentModifyIndex].relations[index];
			}
			index++;
		}

		if(pathToFirstNode || pathToSecondNode)
		{
			console.log("EXISTING PATH");

			var fIndex = graph.nodes[currentModifyIndex].relations.indexOf(pathToFirstNode);
			var sIndex = graph.nodes[currentIndex].relations.indexOf(pathToSecondNode);

			graph.nodes[currentModifyIndex].relations.splice(fIndex, 1);
			graph.nodes[currentIndex].relations.splice(sIndex, 1);
		}
		else
		{
			// Create a path from Node 'A' to Node 'B'
			pathToFirstNode = new NodePath(inWeight, graph.nodes[currentIndex]);
	
			// Create a path from Node 'B' to 'A'
			pathToSecondNode = new NodePath(inWeight, graph.nodes[currentModifyIndex]);	console.log("CREATING PATH");
	
			graph.nodes[currentModifyIndex].relations.push(pathToFirstNode);
			graph.nodes[currentIndex].relations.push(pathToSecondNode);
		}
	
		// It has done its work and can be released.
		curModifyNodeVector = null;
		console.log( pathToSecondNode.weight );
	}

	// Function called when either setting the start node or goal node.
	var setNode = function( type ) {
		var button;
		if( buttonSetNode ) {	// if set already, unset it and set new
			buttonSetNode.style.borderStyle = "outset";
		}
		if( type == "start" ) {
			button = document.getElementById('setStartNode');
			if( buttonSetNode == button ) {
				buttonSetNode = null;
			} else {
				buttonSetNode = button;
				button.style.borderStyle = "inset";
			}
		}
		else if( type == "goal" ) {
			button = document.getElementById('setGoalNode');
			if( buttonSetNode == button ) {
				buttonSetNode = null;
			} else {
				buttonSetNode = button;
				button.style.borderStyle = "inset";
			}
		}
	}

	var sameNode = function(vector)
	{
		if(vector.x != curModifyNodeVector.x)
		{
			return false;
		}
		if(vector.y != curModifyNodeVector.y)
		{
			return false;
		}

		return true;
	}

	var onKeyPress = function(event) {

		// If we are modifying a node
		if(curModifyNodeVector) {

			// Set the node path.
			var nodeIndex = curModifyNodeVector.x+"_"+curModifyNodeVector.y;
			
			if(event.keyCode == 46) 
			{
				 // 46 = Delete
			
		 	 	// Get the node we are going to delete
				var nodeA = graph.nodes[nodeIndex];
			
				// Remove all the relations to node A.
				nodeA.removeRelations();

				// Delete the node itself.
				delete graph.nodes[nodeIndex];
				
				// Reset the modifying state.
				curModifyNodeVector = null;
				
				render();
			}
			else if(event.keyCode == 27) 
			{
				render();
				// 27 = Escape
				curModifyNodeVector = null;
			}
		}
	}

	var onMouseMove = function(event) 
	{
		curMousePosition = getMousePosOnCanvas(canvas, event);
		if(curModifyNodeVector) {
			render();
			renderPathCreation();
		}
	}

	var onMousePress = function(event) 
	{
		/* LEFT CLICK = 1
		 * MIDDLE CLICK = 2
		 * RIGHT CLICK = 3
		 **/
		fixWhich(event);
		
		var mousePosition = getMousePosOnCanvas(canvas, event);
		var validNodePosition = grid.getCellFromCoordinate(mousePosition);		
		
		switch(event.which) 
		{
			case 1:	
				// Inside the grid.
				if(validNodePosition) 
				{
					// The path to current mouse clicks node position on the graph.
					var currentIndex = validNodePosition.x+"_"+validNodePosition.y;

					// If the node allready exists on the graph
					if(graph.nodes[currentIndex]) 
					{
						if( buttonSetNode ) {
							if( buttonSetNode == document.getElementById("setStartNode") ) {
								buttonSetNode.style.borderStyle = "outset";
								buttonSetNode = null;
								startNode.color = defaultNodeColor;
								startNode = graph.nodes[currentIndex];
								startNode.color = startNodeColor;
							}
							else if( buttonSetNode == document.getElementById("setGoalNode") ) {
								buttonSetNode.style.borderStyle = "outset";
								buttonSetNode = null;
								goalNode.color = defaultNodeColor;
								goalNode = graph.nodes[currentIndex];
								goalNode.color = goalNodeColor;
							}
							curModifyNodeVector = null;
						}
						else {
							// If not in modifying mode.
							if(!curModifyNodeVector) 
							{	
								// Track the node.
								curModifyNodeVector = validNodePosition;
							}
							else
							{
						       		if(!sameNode(validNodePosition))	
								{
									editPath(validNodePosition);	
									curModifyNodeVector = null;
								}
							}
						}
					}
					// No node exists where the mouse was clicked.
					else if(graph.nodes[currentIndex] === undefined) 
					{
						// reset start/goal-button
						if( buttonSetNode ) {
							buttonSetNode.style.borderStyle = "outset";
							buttonSetNode = null;
						}

						graph.nodes[currentIndex] = new Node(validNodePosition.x, validNodePosition.y);
					
						if(curModifyNodeVector)
						{
							editPath(validNodePosition);
							curModifyNodeVector = null;
						}	
					}
				}
				break;
			case 2:
				break;
			case 3:
				break;
		}

		render();
	}

	// Resizes the program to fit the screen.
	// (mostly from http://www.html5rocks.com/en/tutorials/casestudies/gopherwoord-studios-resizing-html5-games/)
	var onResize = function() {
		var programArea = document.getElementById('programArea');
		var programWindowRatio = 16 / 9;
		var newWidth = window.innerWidth;
		var newHeight = window.innerHeight;
		var newRatio = newWidth / newHeight;

		if (newRatio > programWindowRatio) {
			newWidth = newHeight * programWindowRatio;
		} else {
			newHeight = newWidth / programWindowRatio;
		}

		programArea.style.height = newHeight + 'px';
		programArea.style.width = newWidth + 'px';
		programArea.style.marginTop = (-newHeight / 2) + 'px';
		programArea.style.marginLeft = (-newWidth / 2) + 'px';

		canvas.width = newWidth;
		canvas.height = newHeight;

		render();
	}
}

// Check if an element exists in an array.
function existsInArray(element, array) {
	for (var index in array) {
		if (array[index] === element) {
			return true;
		}
	}

	return false;
}

// Move all elements in an array to the right from the specified index.
function moveArrayElementsRight(inputArray, fromIndex) {
	for (var index = inputArray.length; index > fromIndex; index--) {
		inputArray[index] = inputArray[index - 1];
	}
}

// A node representing a traversal.
function TraversalNode(original, cameFrom, g_cost, f_cost) {
	this.original = original;
	this.cameFrom = cameFrom;
	this.g_cost = g_cost;
	this.f_cost = f_cost;
}

// Representation of a frontier (basically a priority queue implemented specifically for TraversalNode)
function SearchFrontier() {
	var queue = new Array();

	/** Pushes an object onto the queue at it's prioritized position.
		Returns true if pushed. Returns false if the same object with
		higher priority already exists in the queue. */
	this.push = function(traversalNode) {
		for (var index in queue) {
			// Already in queue with lower cost.
			if (queue[index].original === traversalNode.original && traversalNode.f_cost >= queue[index].f_cost) {
				return false;
			}
		}

		// Increment the index until we find an index with lower cost.
		var index = 0;
		while (index < queue.length && traversalNode.f_cost >= queue[index].f_cost) {
			index++;
		}

		// If we didn't reach the end of the array, we need to move all elements from the index one to the right to make space.
		if (index < queue.length) {
			moveArrayElementsRight(queue, index);
		}

		// Insert the element.
		queue[index] = traversalNode;
		return true;
	}

	// Pop off the element with the highest priority (lowest cost).
	this.pop = function() {
		return queue.shift();
	}

	this.isEmpty = function() {
		if (queue.length == 0) {
			return true;
		} else {
			return false;
		}
	}
}

// Returns an array with the path the search algorithm calculated from the goal traversal node.
function generateTraversalPath(endTraversalNode) {
	var traversalPath = new Array();
	var currentTraversalNode = endTraversalNode;

	while (currentTraversalNode !== null) {
		traversalPath.push(currentTraversalNode.original);
		currentTraversalNode = currentTraversalNode.cameFrom;
	}

	traversalPath.reverse();
	return traversalPath;
}

// Hauristic using the manhattan distance divided by 3 (to be admissible).
function heuristics( inNode, goalNode ) {
	var xDiff = goalNode.col - inNode.col;
	var yDiff = goalNode.row - inNode.row;
	xDiff = Math.abs( xDiff );
	yDiff = Math.abs( yDiff );
	var stepDistance = xDiff + yDiff;
	return (stepDistance / 3);
}

function aStarTreeSeach(startNode, goalNode, heuristicFunction) {
	if (heuristicFunction === undefined) {
		hauristicFunction = function() {
			return 0;
		};
	}

	var frontier = new SearchFrontier();
	frontier.push(new TraversalNode(startNode, null, 0, heuristicFunction(startNode, goalNode)));

	while (frontier.isEmpty() === false) {
		var expanded = frontier.pop();

		// Check if we've reached the goal state.
		if (expanded.original === goalNode) {
			return generateTraversalPath(expanded);
		}

		// Expand node's neighbors into the frontier.
		for (var index in expanded.original.relations) {
			// The actual neighbor node.
			var relation = expanded.original.relations[index].node;

			var g_cost = expanded.g_cost + expanded.original.relations[index].weight;
			var h_cost = heuristicFunction(relation, goalNode);
			var f_cost = g_cost + h_cost;

			// Push if not already there with higher priority.
			frontier.push(new TraversalNode(relation, expanded, g_cost, f_cost));
		}
	}
	return null;
}

function aStarGraphSeach(startNode, goalNode, heuristicFunction) {
	if (heuristicFunction === undefined) {
		hauristicFunction = function() {
			return 0;
		};
	}

	var explored = new Array();
	var frontier = new SearchFrontier();
	frontier.push(new TraversalNode(startNode, null, 0, heuristicFunction(startNode, goalNode)));

	while (frontier.isEmpty() === false) {
		var expanded = frontier.pop();
		explored.push(expanded.original);

		// Check if we've reached the goal state.
		if (expanded.original === goalNode) {
			return generateTraversalPath(expanded);
		}

		// Expand node into the frontier.
		for (var index in expanded.original.relations) {
			var relation = expanded.original.relations[index].node;

			var g_cost = expanded.g_cost + expanded.original.relations[index].weight;
			var h_cost = heuristicFunction(relation, goalNode);
			var f_cost = g_cost + h_cost;
			var wasPushed = false;

			// If the node is not already explored.
			if (existsInArray(relation, explored) === false) {
				// Push if not already there with higher priority.
				frontier.push(new TraversalNode(relation, expanded, g_cost, f_cost));
			}
		}
	}

	return null;
}

function fixWhich(e)
{
	if(!e.which && e.button) {
		if(e.button & 1) {
			e.which = 1;	// Left.
		}
		else if(e.button & 4) {
			e.which = 2;	// Middle.
		}
		else if(e.button & 2) {
			e.which = 3;	// Right.
		}
	}
}

function getMousePosOnCanvas(canvas, event) {
	var rect = canvas.getBoundingClientRect();
	return new Vector(event.clientX - rect.left, event.clientY - rect.top);
}

var assignement = null;

function init() {
		assignement = new Assignment(800, 500)
		assignement.init();
}
window.onload = init;
