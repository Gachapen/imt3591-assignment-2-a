function Vector(x, y) {
	this.x = x;
	this.y = y;
}
Vector.prototype.add = function(vector) {
	this.x += vector.x;
	this.y += vector.y;
	return this;
}
Vector.prototype.subtract = function(vector) {
	this.x -= vector.x;
	this.y -= vector.y;
	return this;
}
Vector.prototype.multiply = function(factor) {
	this.x *= factor;
	this.y *= factor;
	return this;
}
Vector.prototype.reset = function() {
	this.x = 0;
	this.y = 0;
}
Vector.prototype.negate = function() {
	this.x = -this.x;
	this.y = -this.y;
}
Vector.prototype.getDotProduct = function(vector) {
	return this.x * vector.x + this.y * vector.y;
}
Vector.prototype.getReflection = function(normal) {
	var reflection = new Vector(this.x, this.y);
	reflection = reflection.subtract(new Vector(normal.x, normal.y).multiply(2 * this.getDotProduct(normal)));
	return reflection;
}
Vector.prototype.reflect = function(normal) {
	var reflected = this.getReflection(normal);
	this.x = reflected.x;
	this.y = reflected.y;
}
